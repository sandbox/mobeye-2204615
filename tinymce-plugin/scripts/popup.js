var MOBEYE_WIDGET_URL = "http://widget.mobeye.com/app/";
var MOBEYE_API_URL = "http://api.mobeye.com/JsonpApi/";

(function () {
    if (tinyMCEPopup.getWindowArg('widgetId')) {
        loadParameters();
        document.querySelector(".btn-select-mode-back").remove();
        document.getElementById("options").style.display = "block";
    }
    else {
        var startBlock = document.getElementById("select-mobeye-widget-edit-mode");
        startBlock.style.display = "block";
    }
})();

document.getElementById("btn-mobeye-widget-new-email-check").onclick = function(){
    var email = document.querySelector('[name="mobeye-widget-new-email"]');
    if (email.checkValidity()) {
        var script = document.createElement('script');
        script.src = MOBEYE_API_URL + "CheckEmail?callback=checkEmailCallback&email=" + email.value;
        document.body.appendChild(script);
    }
    return false;
};

function checkEmailCallback(data){

    var responseContainer = document.getElementById("mobeye-widget-response");
    responseContainer.textContent = data.Text;

    if (data.Response) {
        responseContainer.style.color = "red";
    }
    else {
        responseContainer.style.color = "green";
    }

    responseContainer.style.display = "block";
    setTimeout(function () {
        responseContainer.style.display = "none";
    }, 5000);
}

document.getElementById("btn-select-mode").onclick = function(){
    var form;
    if (!document.querySelector('[name="load-mobeye-widget-mode"]').checked) {
        form = document.getElementById("options");
    }
    else {
        form = document.getElementById("new-mobeye-widget");
    }
    document.getElementById("select-mobeye-widget-edit-mode").style.display = "none";
    form.style.display = "block";
};

var backButtons = document.querySelectorAll(".btn-select-mode-back");
for (var i = 0; i < backButtons.length; i++) {
    var backButton = backButtons.item(i);
    backButton.onclick = backMethod;
}

function backMethod(){
    var forms = document.querySelectorAll("form");
    for (var i = 0; i < forms.length; i++) {
        var form = forms.item(i);
        form.style.display = "none";
    }

    document.getElementById("select-mobeye-widget-edit-mode").style.display = "block";
}

document.getElementById("save").onclick = function (e) {
    var form = document.getElementById("options");
    if (form.checkValidity()) {
        e.preventDefault();
        var widgetIframe = generateIframe();
        if (tinyMCEPopup.execCommand('mceInsertContent', false, widgetIframe)) {
            tinyMCEPopup.close();
        }
    }
};

document.getElementById("btn-mobeye-widget-new-save").onclick = function (e) {
    var form = document.getElementById("new-mobeye-widget");
    var button = e.target;
    var responseContainer = document.getElementById("mobeye-widget-create-response");
    responseContainer.textContent = "";

    if (form.checkValidity()) {
        button.setAttribute("disabled", "disabled");
        responseContainer.classList.remove("error");
        responseContainer.classList.add("success");
        responseContainer.innerHTML = "Waiting....";
        e.preventDefault();

        var script = document.createElement('script');
        script.src = MOBEYE_API_URL + "CreateWidget?callback=createWidgetCallback" +
            "&email=" + document.querySelector("input[name='mobeye-widget-new-email']").value + "" +
            "&groupName=" + document.getElementById("channel-name").value + "" +
            "&youtubeLink=" + document.querySelector("input[name='youtube-link']").value;
        document.body.appendChild(script);
    }
};

function createWidgetCallback(response) {
    var form = document.getElementById("new-mobeye-widget");

    if (response.Response) {
        form.style.display = "none";
        document.getElementById("wizard-finish").style.display = "block";
        tinyMCEPopup.execCommand('mceInsertContent', false, generateWidgetIframe(response.WidgetId));
    }
    else {
        var responseContainer = document.getElementById("mobeye-widget-create-response");
        responseContainer.classList.remove("success");
        responseContainer.classList.add("error");
        responseContainer.textContent = response.Text;
    }

    document.getElementById("btn-mobeye-widget-new-save").removeAttribute("disabled");
}

document.getElementById("check-channel").onclick = function (e) {
    var channelNameInput = document.getElementById("channel-name");
    if (channelNameInput.checkValidity()) {
        var channelName = document.querySelector("input[name='channel-name']").value;

        var script = document.createElement('script');
        script.src = MOBEYE_API_URL + "CheckIfChannelExists?callback=checkChannelCallback&channelName=" + channelName;
        document.body.appendChild(script);
    }
};

function checkChannelCallback(data) {
    var result = document.getElementById("check-channel-response");

    if (!data) {
        result.classList.remove("error");
        result.classList.add("success");
        result.textContent = "Channel name is free";
    } else {
        result.classList.remove("success");
        result.classList.add("error");
        result.textContent = "Channel is already exists";
    }

    result.style.display = "block";
    setTimeout(function () {
        result.style.display = "none";
    }, 5000);
}

document.getElementById("advanced-button").onclick = function () {
    var youtubeLink = document.getElementById("youtube-link-options");
    youtubeLink.style.display = youtubeLink.style.display === "none" ? "block" : "none";
};

document.querySelector("input[name='mobeye-widget-new-email']").onkeyup = function (e) {
    document.getElementById("channel-name").value = e.target.replace(/@.*/, "") + "_widget";
};

function generateIframe() {
    var queryString = [];
    var optionControls = document.querySelectorAll("input[data-parameter-name]");
    for (var i = 0; i < optionControls.length; i++) {
        var optionControl = optionControls.item(i);
        var optionValue;
        if (optionControl.getAttribute("type") === "checkbox") {
            optionValue = optionControl.checked;
        } else {
            optionValue = optionControl.value;
        }

        queryString.push(optionControl.getAttribute("data-parameter-name") + "=" + optionValue);
    }

    queryString.push('loginButtonText=' + document.getElementById("loginButtonText").value);
    var iframePath = MOBEYE_WIDGET_URL + document.getElementById("widget-id").value + '?' + queryString.join('&');

    var div = document.createElement("div");
    var iframe = document.createElement("iframe");
    iframe.setAttribute("src", iframePath);
    iframe.style.border = 0;
    iframe.style.overflow = "hidden";
    iframe.setAttribute("width", document.getElementById("width").value);
    iframe.setAttribute("height", document.getElementById("height").value);

    div.appendChild(iframe);

    return div.innerHTML;
}

function generateWidgetIframe(widgetId) {
    return "<iframe width='960' height='1000' src='" + MOBEYE_WIDGET_URL + widgetId + "' allowtransparency frameborder='0' scrolling='no'></iframe>";
}

function loadParameters() {

    var optionControls = document.getElementById("options").querySelectorAll("input[data-parameter-name]");

    for (var i = 0; i < optionControls.length; i++) {
        var optionControl = optionControls.item(i);
        var parameterName = optionControl.getAttribute("data-parameter-name");

        if (!tinyMCEPopup.getWindowArg(parameterName)) {
            return;
        }

        if (optionControl.getAttribute("type") === "checkbox") {
            //getWindowArg always returns string
            optionControl.checked = tinyMCEPopup.getWindowArg(parameterName) === "true";
        } else {
            optionControl.value = tinyMCEPopup.getWindowArg(parameterName);
        }
    }

    if (tinyMCEPopup.getWindowArg("widgetId")) {
        document.getElementById("widget-id").value = tinyMCEPopup.getWindowArg("widgetId");
    }

    if (tinyMCEPopup.getWindowArg("width")) {
        document.getElementById("width").value = tinyMCEPopup.getWindowArg("width");
    }

    if (tinyMCEPopup.getWindowArg("height")) {
        document.getElementById("height").value = tinyMCEPopup.getWindowArg("height");
    }

    if (tinyMCEPopup.getWindowArg("loginButtonText")) {
        document.getElementById("loginButtonText").value = tinyMCEPopup.getWindowArg("loginButtonText");
    }
}